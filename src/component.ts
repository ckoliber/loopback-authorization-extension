import { Component, ProviderMap } from "@loopback/core";

import { AuthorizationBindings } from "./keys";
import { AuthorizeActionProvider } from "./providers";

export class AuthorizationComponent implements Component {
    constructor() {}

    providers?: ProviderMap = {
        [AuthorizationBindings.AUTHORIZE_ACTION.key]: AuthorizeActionProvider
    };
}

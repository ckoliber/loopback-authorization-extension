import {
    inject,
    Getter,
    Constructor,
    Provider,
    CoreBindings
} from "@loopback/core";

import { Request } from "@loopback/rest";

import { Where, And, Or, AsyncAuthorizer, AuthorizeFn } from "../types";
import { getAuthorizeMetadata } from "../decorators";

export class AuthorizeActionProvider implements Provider<AuthorizeFn> {
    constructor(
        @inject.getter(CoreBindings.CONTROLLER_CLASS)
        private readonly getController: Getter<Constructor<{}>>,
        @inject.getter(CoreBindings.CONTROLLER_METHOD_NAME)
        private getMethodName: Getter<string>
    ) {}

    async value(): Promise<AuthorizeFn> {
        return async (request, methodArgs) => {
            let controller = await this.getController();
            let methodName = await this.getMethodName();
            let metadata = getAuthorizeMetadata(controller, methodName);

            if (
                !(await this.authorize(
                    metadata,
                    request,
                    controller,
                    methodArgs
                ))
            ) {
                throw {
                    name: "Forbidden",
                    status: 403,
                    message: `Access Forbidden`
                };
            }
        };
    }

    private async authorize(
        where: Where,
        request: Request,
        controller: any,
        args: any[]
    ): Promise<boolean> {
        if (where) {
            if ("and" in where) {
                for (let condition of (where as And).and) {
                    let result = await this.authorize(
                        condition,
                        request,
                        controller,
                        args
                    );

                    // lazy evaluation for high performance
                    if (!result) {
                        return false;
                    }
                }

                return true;
            } else if ("or" in where) {
                for (let condition of (where as Or).or) {
                    let result = await this.authorize(
                        condition,
                        request,
                        controller,
                        args
                    );

                    // lazy evaluation for high performance
                    if (result) {
                        return true;
                    }
                }

                return false;
            } else {
                return await (where as AsyncAuthorizer)(
                    request,
                    controller,
                    args
                );
            }
        }

        return false;
    }
}
